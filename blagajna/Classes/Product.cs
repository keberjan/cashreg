﻿using System;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Windows.Forms;

namespace blagajna
{
    class Product
    {
        SQLiteConnection sqliteconn;
        private bool connectionSuccess;
        private string dbFilename;
        private string appStartupPath;
        private int rows;
        private string prodBarcode;

        public Product()
        {
            dbFilename = "base.db3";
            appStartupPath = Application.StartupPath + @"\" + dbFilename;

            if (!File.Exists(appStartupPath))
            {
                sqliteconn = new SQLiteConnection("Data source=" + appStartupPath + ";Version=3");
                sqliteconn.Open();
                string sql = "CREATE TABLE products (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, name	TEXT NOT NULL UNIQUE, price REAL NOT NULL, tax REAL NOT NULL, barcode TEXT NOT NULL UNIQUE)";
                SQLiteCommand sqlcommand = new SQLiteCommand(sql, sqliteconn);
                sqlcommand.ExecuteNonQuery();
            }
            else
            {
                sqliteconn = new SQLiteConnection("Data source=" + appStartupPath + ";Version=3");
                sqliteconn.Open();
            }

            if(sqliteconn != null && sqliteconn.State == ConnectionState.Open)
            {
                connectionSuccess = true;
            }
        }


        public void addProduct(string _productName, decimal _productPrice, float _ProductTax, string _productbarcode)
        {
            if(_productName == "" || _productPrice.ToString() == "" || _productbarcode == "")
            {
                MessageBox.Show("Fields are empty");
            }

            if(!connectionSuccess)
            {
                MessageBox.Show("Problem with sqlite database. Action aborted.");
                return;
            }

            prodBarcode = _productbarcode;
            if (productExists(prodBarcode))
            {
                MessageBox.Show("Product already exists");
                return;
            }

            try
            {
                string sql = "insert into products (name, price, tax, barcode) values (@productname, @productprice, @tax , @productbarcode)";
                SQLiteCommand sqlcommand = new SQLiteCommand(sql, sqliteconn);
                sqlcommand.Parameters.AddWithValue("@productname", _productName);
                sqlcommand.Parameters.AddWithValue("@productprice", _productPrice);
                sqlcommand.Parameters.AddWithValue("@tax", _ProductTax);
                sqlcommand.Parameters.AddWithValue("@productbarcode", _productbarcode);
                sqlcommand.ExecuteNonQuery();

                MessageBox.Show("Product successfully added!");
                sqliteconn.Close();
            }
            catch (Exception ex)
            {
                Errors.writeException(ex.Message);
                MessageBox.Show(ex.Message);
            }
        }

        public void deleteProduct(string _barcode)
        {
            if (!connectionSuccess)
            {
                MessageBox.Show("Problem with sqlite database. Action aborted.");
                return;
            }

            try
            {
                prodBarcode = _barcode;
                if (!productExists(prodBarcode))
                {
                    MessageBox.Show("Product does not exist");
                    return;
                }

                string sql = "delete from products where barcode = @barcode";
                SQLiteCommand sqlcommand = new SQLiteCommand(sql, sqliteconn);
                sqlcommand.Parameters.AddWithValue("@barcode", _barcode);
                sqlcommand.ExecuteNonQuery();

                MessageBox.Show("Deleted");
                sqliteconn.Close();
            }
            catch (Exception ex)
            {
                Errors.writeException(ex.Message);
                MessageBox.Show("Something happened. Contact your administrator");
            }
        }

        public bool productExists(string _barcode)
        {
            try
            {
                if (!connectionSuccess)
                {
                    Errors.writeException("Problem with sqlite database. Action aborted.");
                    MessageBox.Show("Problem with sqlite database. Action aborted.");
                    return false;
                }

                string sql = "SELECT * FROM products WHERE barcode = @barcode";
                SQLiteCommand sqlcommand = new SQLiteCommand(sql, sqliteconn);
                sqlcommand.Parameters.AddWithValue("@barcode", _barcode);
                rows = Convert.ToInt32(sqlcommand.ExecuteScalar());
                if (rows > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Errors.writeException(ex.Message);
                MessageBox.Show("Error occoured");
                return false;
            }
        }

        public Dictionary<string, object> getProduct(string _barcode)
        {
            if(!connectionSuccess)
            {
                Errors.writeException("Something happened. Contact IT administrator");
                MessageBox.Show("Something happened. Contact IT administrator");
            }

            prodBarcode = _barcode;
            if (!productExists(prodBarcode))
            {
                MessageBox.Show("Product does not exist");
                return null;
            }

            try
            {
                var data = new Dictionary<string, object>();
                string sql = "select * from products where barcode = @barcode";
                SQLiteCommand sqlcommand = new SQLiteCommand(sql, sqliteconn);
                sqlcommand.Parameters.AddWithValue("@barcode", _barcode);
                SQLiteDataReader reader = sqlcommand.ExecuteReader();
                while(reader.Read())
                {
                    data.Add("name", reader["name"]);
                    data.Add("price", reader["price"]);
                    data.Add("tax", reader["tax"]);
                    data.Add("barcode", reader["barcode"]);
                }

                return data;
            }
            catch (Exception ex)
            {
                Errors.writeException(ex.Message);
                MessageBox.Show("Something happened. Contact IT administrator");
                return null;
            }
        }
    }
}
