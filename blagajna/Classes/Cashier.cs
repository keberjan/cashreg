﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using blagajna.UI;

namespace blagajna.Classes
{
    class Cashier
    {
        private blagForm ui;
        private bool dup;
        private decimal total;

        public Cashier(blagForm ui)
        {
            this.ui = ui;
        }

        public void scanAddProduct(string _barcode)
        {
            findDuplicate(_barcode);
            if (!dup)
            {
                Product product = new Product();
                var data = product.getProduct(_barcode);
                string pName = data["name"].ToString();
                decimal pPrice = Convert.ToDecimal(data["price"]);
                decimal pTax = Convert.ToDecimal(data["tax"]);
                decimal taxCalculated = (pTax / 100) + 1;
                decimal finalPrice = decimal.Multiply(pPrice, taxCalculated);
                string pBarcode = data["barcode"].ToString();

                ui.blagajnaGrid.Rows.Add(pName, pPrice, pTax, "1", pBarcode, pPrice, finalPrice);
                totalPrice();
            }
        }

        public bool findDuplicate(string _barcode)
        {
            dup = false;
            foreach (DataGridViewRow row in ui.blagajnaGrid.Rows)
            {
                string barcode = Convert.ToString(row.Cells["gProductBarcode"].Value);
                if(barcode == _barcode)
                {
                    decimal productTaxCell = Convert.ToDecimal(row.Cells["gProductTax"].Value);
                    decimal taxCalculated = (productTaxCell / 100) + 1;

                    dup = true;
                    int cellvalue = Convert.ToInt32(row.Cells["gProductQuantity"].Value);
                    row.Cells["gProductQuantity"].Value = cellvalue = cellvalue + 1;

                    decimal productPrice = Convert.ToDecimal(row.Cells["gProductPrice"].Value);
                    int productQuantity = Convert.ToInt32(row.Cells["gProductQuantity"].Value);
                    decimal finalPrice = productPrice * productQuantity;
                    row.Cells["gPriceWithoutTax"].Value = finalPrice;
                    decimal finalPriceWithTax = decimal.Multiply(finalPrice, taxCalculated);
                    row.Cells["gFinalPrice"].Value = finalPriceWithTax;
                    totalPrice();
                }
            }
            return dup;
        }

        public void totalPrice()
        {
            total = 0;

            foreach (DataGridViewRow row in ui.blagajnaGrid.Rows)
            {
                decimal price = Convert.ToDecimal(row.Cells["gFinalPrice"].Value);
                total = total + price;
                ui.textboxTotal.Text = Convert.ToString(total);
            }
        }

        public void cellChange()
        {
            foreach (DataGridViewRow row in ui.blagajnaGrid.Rows)
            {
                decimal productTaxCell = Convert.ToDecimal(row.Cells["gProductTax"].Value);
                decimal taxCalculated = (productTaxCell / 100) + 1;

                decimal productPrice = Convert.ToDecimal(row.Cells["gProductPrice"].Value);
                int productQuantity = Convert.ToInt32(row.Cells["gProductQuantity"].Value);
                decimal finalPrice = productPrice * productQuantity;
                row.Cells["gPriceWithoutTax"].Value = finalPrice;
                decimal finalPriceWithTax = decimal.Multiply(finalPrice, taxCalculated);
                row.Cells["gFinalPrice"].Value = finalPriceWithTax;
            }
            totalPrice();
        }
    }
}
