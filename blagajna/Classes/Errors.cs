﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace blagajna
{
    class Errors
    {
        public static void writeException(string _exception)
        {
            using (StreamWriter writetext = new StreamWriter("exceptions.log"))
            {
                writetext.WriteLine(_exception);
            }

        }
    }
}
