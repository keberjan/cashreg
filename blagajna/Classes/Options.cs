﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blagajna.Classes
{
    public class Options
    {
        public float taxOne;
        public float taxTwo;

        public void setTaxOne(float _taxOne)
        {
            taxOne = _taxOne;
        }

        public float getTaxOne()
        {
            return taxOne;
        }

        public void setTaxTwo(float _taxTwo)
        {
            taxTwo = _taxTwo;
        }

        public float getTaxTwo()
        {
            return taxTwo;
        }
    }
}
