﻿namespace blagajna.UI
{
    partial class addProductForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtAddProductName = new System.Windows.Forms.TextBox();
            this.txtAddProductPrice = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtAddProductBarcode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnAddProduct = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.comboTax = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Product name:";
            // 
            // txtAddProductName
            // 
            this.txtAddProductName.Location = new System.Drawing.Point(51, 33);
            this.txtAddProductName.Name = "txtAddProductName";
            this.txtAddProductName.Size = new System.Drawing.Size(177, 20);
            this.txtAddProductName.TabIndex = 1;
            // 
            // txtAddProductPrice
            // 
            this.txtAddProductPrice.Location = new System.Drawing.Point(51, 145);
            this.txtAddProductPrice.Name = "txtAddProductPrice";
            this.txtAddProductPrice.Size = new System.Drawing.Size(177, 20);
            this.txtAddProductPrice.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(48, 128);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Product price:";
            // 
            // txtAddProductBarcode
            // 
            this.txtAddProductBarcode.Location = new System.Drawing.Point(51, 202);
            this.txtAddProductBarcode.Name = "txtAddProductBarcode";
            this.txtAddProductBarcode.Size = new System.Drawing.Size(177, 20);
            this.txtAddProductBarcode.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(48, 185);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Product barcode:";
            // 
            // btnAddProduct
            // 
            this.btnAddProduct.Location = new System.Drawing.Point(79, 249);
            this.btnAddProduct.Name = "btnAddProduct";
            this.btnAddProduct.Size = new System.Drawing.Size(128, 48);
            this.btnAddProduct.TabIndex = 6;
            this.btnAddProduct.Text = "AddProduct";
            this.btnAddProduct.UseVisualStyleBackColor = true;
            this.btnAddProduct.Click += new System.EventHandler(this.btnAddProduct_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(48, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Product tax:";
            // 
            // comboTax
            // 
            this.comboTax.FormattingEnabled = true;
            this.comboTax.Location = new System.Drawing.Point(51, 88);
            this.comboTax.Name = "comboTax";
            this.comboTax.Size = new System.Drawing.Size(177, 21);
            this.comboTax.TabIndex = 8;
            // 
            // addProductForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(281, 343);
            this.Controls.Add(this.comboTax);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnAddProduct);
            this.Controls.Add(this.txtAddProductBarcode);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtAddProductPrice);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtAddProductName);
            this.Controls.Add(this.label1);
            this.Name = "addProductForm";
            this.Text = "CashReg - Add Product";
            this.Load += new System.EventHandler(this.addProductForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtAddProductName;
        private System.Windows.Forms.TextBox txtAddProductPrice;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtAddProductBarcode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnAddProduct;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboTax;
    }
}