﻿using blagajna.UI;
using System;
using System.Windows.Forms;
using System.Xml;
using blagajna.Classes;

namespace blagajna
{
    public partial class MainForm : Form
    {
        private Options options;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            XmlDocument xml = new XmlDocument();
            xml.Load("settings.xml");

            XmlNode TaxesNode = xml.SelectSingleNode("/taxes");
            float tax1 = float.Parse(TaxesNode.SelectSingleNode("tax1").InnerText);
            float tax2 = float.Parse(TaxesNode.SelectSingleNode("tax2").InnerText);

            options = new Options();
            options.setTaxOne(tax1);
            options.setTaxTwo(tax2);
        }

        private void addProductToolStripMenuItem_Click(object sender, EventArgs e)
        {
            addProductForm window = new addProductForm(options);
            window.ShowDialog();
        }

        private void deleteProductToolStripMenuItem_Click(object sender, EventArgs e)
        {
            f_deleteproduct window = new f_deleteproduct();
            window.ShowDialog();
        }

        private void blagajnaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            blagForm window = new blagForm();
            window.ShowDialog();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About window = new About();
            window.ShowDialog();
        }
    }
}
