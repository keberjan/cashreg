﻿using System;
using System.Windows.Forms;

namespace blagajna
{
    public partial class f_deleteproduct : Form
    {
        public f_deleteproduct()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string productName = textBox_productname.Text;
            Product product = new Product();
            product.deleteProduct(productName);
        }
    }
}
