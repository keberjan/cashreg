﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using blagajna.Classes;

namespace blagajna.UI
{
    public partial class addProductForm : Form
    {
        private Options options;

        public addProductForm(Options options)
        {
            InitializeComponent();
            this.options = options;
        }

        private void addProductForm_Load(object sender, EventArgs e)
        {
            float taxOne = options.getTaxOne();
            float taxTwo = options.getTaxTwo();
            comboTax.Items.Add(taxOne);
            comboTax.Items.Add(taxTwo);
        }

        private void btnAddProduct_Click(object sender, EventArgs e)
        {
            string productName = txtAddProductName.Text;
            decimal productPrice = decimal.Parse(txtAddProductPrice.Text);
            float tax = float.Parse(comboTax.Text);
            string productBarcode = txtAddProductBarcode.Text;

            Product product = new Product();
            product.addProduct(productName, productPrice, tax, productBarcode);
        }
    }
}
