﻿using System;
using System.Windows.Forms;
using blagajna.Classes;

namespace blagajna.UI
{
    public partial class blagForm : Form
    {
        public blagForm()
        {
            InitializeComponent();
        }

        private void BlagajnaAddButton_Click(object sender, EventArgs e)
        {
            string barcode = blagajnaBarText.Text;
            Cashier cashier = new Cashier(this);
            cashier.scanAddProduct(barcode);
        }

        private void blagajnaGrid_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            Cashier cashier = new Cashier(this);
            cashier.cellChange();
        }
    }
}
