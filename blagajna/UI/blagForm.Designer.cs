﻿namespace blagajna.UI
{
    partial class blagForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.blagajnaGrid = new System.Windows.Forms.DataGridView();
            this.BlagajnaAddButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.blagajnaBarText = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textboxTotal = new System.Windows.Forms.TextBox();
            this.gProductName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gProductPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gProductTax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gProductQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gProductBarcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gPriceWithoutTax = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gFinalPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.blagajnaGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // blagajnaGrid
            // 
            this.blagajnaGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.blagajnaGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gProductName,
            this.gProductPrice,
            this.gProductTax,
            this.gProductQuantity,
            this.gProductBarcode,
            this.gPriceWithoutTax,
            this.gFinalPrice});
            this.blagajnaGrid.Location = new System.Drawing.Point(12, 47);
            this.blagajnaGrid.Name = "blagajnaGrid";
            this.blagajnaGrid.Size = new System.Drawing.Size(928, 378);
            this.blagajnaGrid.TabIndex = 0;
            this.blagajnaGrid.CurrentCellDirtyStateChanged += new System.EventHandler(this.blagajnaGrid_CurrentCellDirtyStateChanged);
            // 
            // BlagajnaAddButton
            // 
            this.BlagajnaAddButton.Location = new System.Drawing.Point(176, 11);
            this.BlagajnaAddButton.Name = "BlagajnaAddButton";
            this.BlagajnaAddButton.Size = new System.Drawing.Size(75, 23);
            this.BlagajnaAddButton.TabIndex = 1;
            this.BlagajnaAddButton.Text = "Enter";
            this.BlagajnaAddButton.UseVisualStyleBackColor = true;
            this.BlagajnaAddButton.Click += new System.EventHandler(this.BlagajnaAddButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Barcode:";
            // 
            // blagajnaBarText
            // 
            this.blagajnaBarText.Location = new System.Drawing.Point(70, 12);
            this.blagajnaBarText.Name = "blagajnaBarText";
            this.blagajnaBarText.Size = new System.Drawing.Size(100, 20);
            this.blagajnaBarText.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 438);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Total price:";
            // 
            // textboxTotal
            // 
            this.textboxTotal.Location = new System.Drawing.Point(115, 439);
            this.textboxTotal.Name = "textboxTotal";
            this.textboxTotal.Size = new System.Drawing.Size(100, 20);
            this.textboxTotal.TabIndex = 5;
            // 
            // gProductName
            // 
            this.gProductName.HeaderText = "Product Name";
            this.gProductName.Name = "gProductName";
            // 
            // gProductPrice
            // 
            this.gProductPrice.HeaderText = "Product Price";
            this.gProductPrice.Name = "gProductPrice";
            this.gProductPrice.ReadOnly = true;
            // 
            // gProductTax
            // 
            this.gProductTax.HeaderText = "Tax %";
            this.gProductTax.Name = "gProductTax";
            this.gProductTax.ReadOnly = true;
            // 
            // gProductQuantity
            // 
            this.gProductQuantity.HeaderText = "Quantity";
            this.gProductQuantity.Name = "gProductQuantity";
            // 
            // gProductBarcode
            // 
            this.gProductBarcode.HeaderText = "Product Barcode";
            this.gProductBarcode.Name = "gProductBarcode";
            // 
            // gPriceWithoutTax
            // 
            this.gPriceWithoutTax.HeaderText = "Price without tax";
            this.gPriceWithoutTax.Name = "gPriceWithoutTax";
            this.gPriceWithoutTax.ReadOnly = true;
            // 
            // gFinalPrice
            // 
            this.gFinalPrice.HeaderText = "Final Price";
            this.gFinalPrice.Name = "gFinalPrice";
            // 
            // blagForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(952, 505);
            this.Controls.Add(this.textboxTotal);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.blagajnaBarText);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BlagajnaAddButton);
            this.Controls.Add(this.blagajnaGrid);
            this.Name = "blagForm";
            this.Text = "Blagajna";
            ((System.ComponentModel.ISupportInitialize)(this.blagajnaGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.DataGridView blagajnaGrid;
        private System.Windows.Forms.Button BlagajnaAddButton;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox blagajnaBarText;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox textboxTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn gProductName;
        private System.Windows.Forms.DataGridViewTextBoxColumn gProductPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn gProductTax;
        private System.Windows.Forms.DataGridViewTextBoxColumn gProductQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn gProductBarcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn gPriceWithoutTax;
        private System.Windows.Forms.DataGridViewTextBoxColumn gFinalPrice;
    }
}